cmds="add commit push status tig Quit "
PS3="Select a git command: "
select cmd in $cmds
do
	if [ $cmd == 'Quit' ]
	then
	  break
	fi
	if [ $cmd == 'add'  ]
	then	
	git $cmd .
	echo "Add to Repos"
        git status
	elif [ $cmd == 'commit' ]
	then
	git $cmd -m "Committed"
	echo "committed"
	git status
	elif [ $cmd == 'push' ]
	then
	git $cmd origin master
	echo "Pushed"
	git status
	elif [ $cmd == 'status' ]
	then
	git $cmd
	elif [ $cmd == 'tig' ]
	then
	tig
	fi	
done
echo "Git Process Over"

