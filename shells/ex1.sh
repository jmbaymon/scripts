#!/bin/bash
# Create a script in your $HOME/bin directory called myownscript. When the
# script runs, it should output information that looks as follows:
# Today is Sat Dec 10 15:45:04 EST 2011.
# You are in /home/joe and your host is abc.example.com.
# Of course, you need to read in your current date/time, current working directory,
# and hostname. Also, include comments about what the script does and indicate
# that the script should run with the /bin/bash shell.
today=$(date)
path1=$(pwd)
hoster=$(hostname)
echo "Today is $today"
echo "You are in $path1 and your host is $hoster"
